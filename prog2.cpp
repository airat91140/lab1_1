#include <iostream>
#include <prog1.h>

using namespace std;
using namespace prog1;

Matrix *input(int &m, int &n) { //function for matrix input.
    Matrix *matrix;
    int num = 1;
    //char err[] = "";
    string err = "";
    m = n = -1;
    do {
        cout<<err;
        cout<<"Enter number of lines:"<<endl;
        getNum(m);
        cout<<"Enter length of a line:"<<endl;
        getNum(n);
        err = "You are wrong. Repeat, please.\n";
    }while(m <= 0 || n <= 0);

    try {
        matrix = new Matrix;
        matrix->line = new Elem *[m];
    }
    catch(bad_alloc &er) {
        cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
        m = n = 0;
        return nullptr;
    }

    for (int i = 0; i < m; ++i) {
        matrix->line[i] = nullptr;
    }

    cout<<"Enter line = -1, if you want to stop"<<endl;
    int line = 0;
    while (true) {
        int index;
        do {
            cout<<"Enter line:"<<endl;
            cin>>line;
        } while(line < -1 || line >= m);
        if (line == -1) {
            break;
        }
        do {
            cout<<"Enter index:"<<endl;
            cin>>index;
        } while (index < 0 || index > n);

        cout<<"Enter number:"<<endl;
        int num;
        getNum(num);
        if (matrix->line[line] == nullptr) { //случай с пустой строкой
            try {
                matrix->line[line] = new Elem;
                matrix->line[line]->num = num;
                matrix->line[line]->index = index;
                matrix->line[line]->next = nullptr;
            }
            catch(bad_alloc &er) {
                cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
                m = n = 0;
                return nullptr;
            }
        }
        Elem *elem = matrix->line[line];
        if (elem->index > index) { //случай с наименьшим индексом
            try {
                matrix->line[line] = new Elem;
                matrix->line[line]->num = num;
                matrix->line[line]->index = index;
                matrix->line[line]->next = elem;
                continue;
            }
            catch (bad_alloc &er) {
                cout << "Oh, it seems we have not memory enough. There is an error code: " << er.what() << endl;
                m = n = 0;
                return nullptr;
            }
        }
        while (elem != nullptr && elem->next != nullptr) {
            if (elem->index == index) { //случай перезаписи
                elem->num = num;
                elem = nullptr;
                break;
            }
            if (elem->index < index && elem->next->index > index) { //нормальный случай
                Elem *tmp = elem->next;
                try {
                    tmp = new Elem;
                    tmp->num = num;
                    tmp->index = index;
                    tmp->next = elem->next;
                    elem->next = tmp;
                    elem = nullptr;
                    break;
                }
                catch(bad_alloc &er) {
                    cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
                    m = n = 0;
                    return nullptr;
                }
            }
            elem = elem->next;
        }
        if (elem != nullptr && index > elem->index) {
            try {
                elem->next = new Elem;
                elem->next->num = num;
                elem->next->index = index;
                elem->next->next = nullptr;
            }
            catch (bad_alloc &er) {
                cout << "Oh, it seems we have not memory enough. There is an error code: " << er.what() << endl;
                m = n = 0;
                return nullptr;
            }
        }
    }
    return matrix;
}

void show(Matrix *matrix, int m, int n) { //matrix output
    for (int i = 0; i < m; ++i) {
        Elem *elem = matrix->line[i];
        while (elem != nullptr) {
            cout << "(" << i << ";" << elem->index << ")" << elem->num << " ";
            elem = elem->next;
        }
        cout<<endl;
    }
}

int findmax(Elem *first, int m, int n) { //function for searching max amount of equal elements in a line
    int max = 0, count;
    Elem *elem;
    while (first != nullptr) {
        count = 1;
        elem = first->next;
        while (elem != nullptr) {
            if (first->num == elem->num)
                ++count;
            elem = elem->next;
        }
        if (count > max)
            max = count;
        first = first->next;
    }
    return max;
}

int *formVector(Matrix *matrix, int m, int n) {
    int *vector;
    try {
        vector = new int[m];
    }
    catch (bad_alloc &er) {
        cout << "Oh, it seems we have not memory enough. There is an error code: " << er.what() << endl;
        return nullptr;
    }
    for (int i = 0; i < m; ++i) {
        vector[i] = findmax(matrix->line[i], m, n);
    }
    return vector;
}

void erase(Matrix *matrix, int m, int n) {
    for (int i = 0; i < m; ++i) {
        Elem *first = matrix->line[i];
        while (first) {
            Elem *temp;
            temp = first->next;
            delete first;
            first = temp;
        }
    }
    delete[] matrix->line;
    delete matrix;
}