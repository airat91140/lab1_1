#include <iostream>
#include "prog2.cpp"

using namespace prog1;

int main() {
    //int **matrix = nullptr;
    Matrix *matrix;
    int m, n, *vector;
    matrix = input(m, n);
    cout<<"Formed matrix:"<<endl;
    show(matrix, m, n);
    vector = formVector(matrix, m, n);
    cout<<"formed vector:"<<endl;
    for (int i = 0; i < m; ++i) {
        cout<<vector[i]<<endl;
    }
    erase(matrix, m ,n);
    delete[] vector;
    return 0;
}
