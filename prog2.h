#include <iostream>
using namespace std;

namespace prog2 {
    template<class T> //function for safe inputting a number. returns 0 if all is ok.
    void getNum(T &a) {
        while (true) {
            cin >> a;
            if (cin.good()) {
                return;
            }
            cout << "You are wrong. Repeat, please." << endl;
            cin.clear();
            cin.ignore(32767,'\n');
        }
    }
}