#include "prog1.h"
using namespace std;
using namespace prog1;

int **input(int &m, int &n) { //function for matrix input.
    int **matrix;
    char *err = (char *)"";
    m = n = -1;
    do {
        cout<<err;
        cout<<"Enter number of lines:"<<endl;
        getNum(m);
        cout<<"Enter length of a line:"<<endl;
        getNum(n);
        err = (char *)"You are wrong. Repeat, please.\n";
    }while(m <= 0 || n <= 0);

    try {
        matrix = new int *[m];
    }
    catch(bad_alloc &er) {
        cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
        m = n = 0;
        return nullptr;
    }

    for (int i = 0; i < m; ++i) {
        cout<<"Line #"<<i<<endl;
        try {
            matrix[i] = new int[n];
        }
        catch(bad_alloc &er) {
            cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
            m = n = 0;
            return nullptr;
        }
        cout<<"Enter numbers:"<<endl;
        for (int j = 0; j < n; ++j) {
            getNum(matrix[i][j]);
        }
    }
    return matrix;
}

void show(int **matrix, int m, int n) { //matrix output
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << matrix[i][j] << " ";
        }
        cout<<endl;
    }
}

int findmax(int *line, int m, int n) { //function for searching max amount of equal elements in a line
    int max = 0, count;
    for (int i = 0; i < n; ++i) {
        if (line[i] == 0)
            continue;
        count = 1;
        for (int j = i + 1; j < n; ++j) {
            if (line[i] == line[j])
                ++count;
        }
        if (count > max)
            max = count;
    }
    return max;
}

int *formVector(int **matrix, int m, int n) {
    int *vector;
    try {
        vector = new int[m];
    }
    catch(bad_alloc &er) {
        cout<<"Oh, it seems we have not memory enough. There is an error code: "<<er.what()<<endl;
        return nullptr;
    }
    for (int i = 0; i < m; ++i) {
        vector[i] = findmax(matrix[i], m, n);
    }
    return vector;
}

void erase(int **matrix, int m, int n) {
    for (int i = 0; i < m; ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}